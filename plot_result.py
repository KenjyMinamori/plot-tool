"""
    Plot every data
"""
import os
import matplotlib.pyplot as plt
import numpy as np



def save_img(path, name='', fmt='png'):
    """
        _save image with given path and name.
    """
    try:
        pwd = os.getcwd()
        try:
            os.mkdir(path + fmt)
        except FileExistsError:
            pass
        except Exception:
            print('Error: %s' % Exception)
            return False
        finally:
            os.chdir(path + fmt)
            plt.savefig('%s.%s' % (name, fmt), fmt='png', dpi=150)
            os.chdir(pwd)
            return '%s%s/%s.%s' % (path, fmt, name, fmt)
    except Exception:
        print('Error wile saving')
        return False



def plot_data(all_data, PATH, name):
    """ Build plot and save in the end """
    all_data.charge_sum = all_data.after_charge_mah + all_data.charge_mah + all_data.pre_charge_mah
    fig, ax1 = plt.subplots()

    ax1.plot(all_data.time, all_data.current, 'b-')
    ax1.set_xlabel('time (h)')

    ax1.set_ylabel('current (mA)', color='b')
    for tick in ax1.get_yticklabels():
        tick.set_color('b')

    x_min = 0
    x_max = 5
    y_min = 0
    y_max = 1000

    major_ticks_ax1 = np.arange(0, y_max, y_max/10)
    minor_ticks_ax1 = np.arange(0, y_max, y_max/20)

    plt.axis([x_min, x_max, y_min, y_max])

    # Now make second axes
    ax2 = ax1.twinx()

    ax2.plot(all_data.time, all_data.voltage, 'r--')
    ax2.set_ylabel('voltage (mV)', color='r')
    for tick in ax2.get_yticklabels():
        tick.set_color('r')

    plt.text(6, 2000, "  pre-charge (mAh): %.2f" % all_data.pre_charge_mah,
             ha='left', color="grey", family="monospace", size=10)

    plt.text(6, 1850, "      charge (mAh): %.2f" % all_data.charge_mah,
             ha='left', color="grey", family="monospace", size=10)

    plt.text(6, 1700, "after-charge (mAh): %.2f" % all_data.after_charge_mah,
             ha='left', color="grey", family="monospace", size=10)

    plt.text(6, 1550, "     charged (mAh): %.2f" % all_data.charge_sum,
             ha='left', color="red", family="monospace", size=10)

    plt.text(6, 1400, "  discharged (mAh): %.2f" % all_data.discharge_mah,
             ha='left', color="green", family="monospace", size=10)

    #ax = ax1.twinx()
    # major ticks every 20, minor ticks every 5
    x_min = 0
    x_max = 10
    y_min = 0
    y_max = 5000

    major_ticks = np.arange(0, y_max, y_max/20)
    minor_ticks = np.arange(0, y_max, y_max/20)

    major_ticksx = np.arange(0, 10, 1)
    minor_ticksx = np.arange(0, 10, 0.2)

    ax1.set_xticks(major_ticksx)
    ax1.set_xticks(minor_ticksx, minor=True)
    ax1.set_yticks(major_ticks_ax1)
    ax1.set_yticks(minor_ticks_ax1, minor=True)
    ax2.set_yticks(major_ticks)
    ax2.set_yticks(minor_ticks, minor=True)

    ax1.grid(which='minor', alpha=0.4)
    ax1.grid(which='major', alpha=0.9)

    ax2.grid(which='minor', alpha=0.2)
    ax2.grid(which='major', alpha=0.3)


    plt.axis([x_min, x_max, y_min, y_max])
    #ax1.grid(which = "both")
    #ax2.grid(which = "both")
    #ax1.yaxis.grid(False)

    image_name = save_img(PATH, name)
    plt.close()
    return image_name
    #logRecordToDB(c, name, path, all_data)
