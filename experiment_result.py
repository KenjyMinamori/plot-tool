"""
    Contain class all data and some functions for parsing
"""
import os
from datetime import datetime
import csv
import numpy as np


def read_float(val):
    """ replace . with , for real numbers """
    return np.float64(float(val.replace(',', '.')))


def parse_list(files, dbase):
    """ Return parsed data """
    print('Parsing %s to %s' %(files, dbase))
    return


class MyParser():
    """
    Data holder or something like this. In JS it would be more straight i think
    """
    def __init__(self, dbase, path, name):
        # ValueException's counter
        self.errors = 0

        # Let it be
        self.full_path = path + name

        # Data Base
        self.cur = dbase.cursor()

        # Main data !
        self.time = np.array([0.0])
        self.current = np.array([0.0])
        self.voltage = np.array([0.0])

        # Date of experiment start
        self.date = datetime.fromtimestamp(
            os.path.getmtime(path + name)).strftime('%Y-%m-%d %H:%M:%S')

        # Name parser (it contains some info)
        data_from_name = name.split('-')
        # Name of accum
        self.accum_name = data_from_name[0]
        # Kind of experiment (only 'zr' now)
        self.kind_of_experiment = data_from_name[1]

        # Dictionary with status id's like discharge:1 charge:2 and so on
        self.status_dict = {}
        self.gen_status_list()
        if self.kind_of_experiment == 'zr' or 'rz':
            self.charge_current = data_from_name[3]
            self.discharge_current = data_from_name[5]
            self.pre_charge_mah = 0
            self.charge_mah = 0
            self.charge_sum = 0
            self.after_charge_mah = 0
            self.discharge_mah = 0
            #hardcode
            self.max_v = 4200
            self.min_v = 3000
            self.warning = 0
            self.after_charge = 0

            self.create_record_for_experiment()
            self.parse_file()
        else:
            pass

    def create_record_for_experiment(self):
        """ Creates record for experiment and set's it's id
            If it's not exists then delete all data and PARSE AGAIN
            BWAHAHAHAHA
            TODO: fix that
        """
        #Check if it's not exists
        self.cur.execute("""
                         SELECT id FROM experiments WHERE path = '%s'
                         """ % self.full_path)
        self.data = self.cur.fetchall()
        if self.data:
            if self.data[0][0]:
                self.experiment_id = self.data[0][0]
                print (self.experiment_id)
                self.cur.execute('''DELETE FROM logs WHERE experiment_id = %s''' % (self.experiment_id))
                self.cur.execute('''DELETE FROM results WHERE experiment_id = %s''' % (self.experiment_id))
            else:
                raise Warning("Data from DB is wrong while SELECT experimet_id")
        else:
            print('Creating new record for csv')
            self.cur.execute("""
                        INSERT INTO experiments(accum_id, name, path, date) 
                        VALUES (?,?,?,?)
                        """, (self.accum_name, self.kind_of_experiment,
                              self.full_path, self.date))
            self.cur.execute("""SELECT last_insert_rowid()""")
            #dbase.commit()
            self.experiment_id = self.cur.fetchall()
            self.experiment_id = self.experiment_id[0][0]

    def valid_data(self):
        """ Validate data. Also make simple analysys of warn"""
        if self.after_charge_mah > 0:
            self.after_charge = 1
        try:
            diff = self.charge_sum - self.discharge_mah
            if diff > 0:
                diff = diff / self.charge_sum
            elif diff < 0:
                diff = abs(diff) / self.discharge_mah
            else:
                self.warning = 1
                return False

            if diff >= 0.2:
                self.warning = 1
                return False
        except ZeroDivisionError:
            self.warning = 1
            return False

    def gen_status_list(self):
        ''' make a dictionary with status id (discharge, charge etc)'''
        self.cur.execute("SELECT name, id FROM status_list")
        status_list = self.cur.fetchall()
        for i in status_list:
            self.status_dict[i[0]] = i[1]

    def record_success(self, dbase, name):
        """ Check that all is cool"""
        print('NAME IS %s' % name)
        self.cur.execute("UPDATE experiments SET image = (?) WHERE id = (?)",
                         (name, self.experiment_id))
        dbase.commit()
        return True


    def parse_file(self):
        """ Parse file and write all logs to db"""
        lastdischarge = 0
        step = 0
        last_time = 0
        current_now = 0

        with open(self.full_path, 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

            first_line = True
            last_time = 0
            start_time = 0
            place_dot_step = 2000
            i = 1
            for row in spamreader:
                try:
                    if i == place_dot_step:
                        i = 1
                        print('.', end="", flush=True)
                    i += 1
                    time_now = read_float(row[0])

                    if first_line:
                        start_time = time_now
                        first_line = False

                    time_now -= start_time
                    integer_time = time_now
                    time_now = time_now/(1000 * 3600)

                    current_now = read_float(row[1])
                    voltage_now = read_float(row[2])
                    status = row[3]


                    lastdischarge = step
                    step = (time_now - last_time)

                    # i catched mysterious bug here and only this shit can help (sorry)
                    if ("%s" % step) == 'nan':
                        step = lastdischarge
                    last_time = time_now
                    self.cur.execute("""INSERT INTO
                                        logs(current, voltage, time, step, experiment_id) 
                                        VALUES (?, ?, ?, ?, ?)
                                     """, (current_now * 100,
                                           voltage_now * 100,
                                           integer_time,
                                           self.status_dict[status],
                                           self.experiment_id))

                    if status == "precharge":
                        self.pre_charge_mah += current_now * step
                    elif status == "charge":
                        self.charge_mah += current_now * step
                    elif status == "aftercharge":
                        self.after_charge_mah += current_now * step
                    elif status == "discharge":
                        self.discharge_mah += current_now * step

                    self.time = np.append(self.time, time_now)
                    self.current = np.append(self.current, current_now)
                    self.voltage = np.append(self.voltage, voltage_now)
                except ValueError:
                    self.errors += 1
        self.charge_sum = self.pre_charge_mah + self.charge_mah + self.after_charge_mah
        self.valid_data()
        print("validated: ch %s di %s w %s" % (self.charge_mah, self.discharge_mah, self.warning))
        self.cur.execute("""INSERT INTO
                            results(experiment_id, precharge, charge, aftercharge, discharge, maxv, minv, warn,
                            charge_current, discharge_current) 
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                          """, (self.experiment_id,
                                self.pre_charge_mah,
                                self.charge_mah,
                                self.after_charge_mah,
                                self.discharge_mah,
                                self.max_v,
                                self.min_v,
                                self.warning,
                                self.charge_current,
                                self.discharge_current))


def main():
    """ this uses to test module"""
    pass

if __name__ == "__main__":
    main()
