"""
    Finds needed csv files in path
"""
import os

def filter_result(dbase, files, path):
    """ Remove all shit from list """
    logs = list(filter(lambda x: x.endswith('.csv'), files))

    existing_images = os.listdir(path + "/png/")
    existing_images = list(filter(lambda x: x.endswith('.png'), existing_images))

    parsed_logs = dbase.cursor()
    parsed_logs.execute(''' SELECT path, image FROM experiments WHERE IMAGE IS NOT NULL''')
    parsed_logs = parsed_logs.fetchall()

    true_parsed_logs = []
    for experiment in parsed_logs:
        experiment_path = experiment[0]
        image = experiment[1]

        for existing_image in existing_images:
            if image == path + "png/" + existing_image:
                true_parsed_logs.append(experiment_path)



    answer = []
    for log in logs:
        good = True
        for dbrec in true_parsed_logs:
            #print('filering. what we got: %s \n what in db:%s \n' %(path + log, dbrec[0]))
            if path + log == dbrec:
                good = not good
        if good:
            answer.append(log)

    return answer

def get_list_of_csv_files(dbase, path):
    """  Return list of needed files """
    files = os.listdir(path)
    answer = filter_result(dbase, files, path)
    print(answer)
    return answer
