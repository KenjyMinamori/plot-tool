"""
    Parse and plot data. DB is used
"""
import sys
import sqlite3
from csv_finder import get_list_of_csv_files
import experiment_result as expr
from plot_result import plot_data

#conn = sqlite3.connect('experiments2.db')
#c = conn.cursor()
#c.execute('''SELECT * FROM experiments''')
#print (c.fetchall())
#conn.close()

#connect to data base
DBASE = sqlite3.connect('exp2.db')

#get path
if len(sys.argv) == 1:
    PATH = "./logs/rob-zr/"
    print("Choosen path is '%s'" % PATH)
else:
    PATH = sys.argv[1] + "/"

#get list of files needed to be parsed
LIST_OF_FILES = get_list_of_csv_files(DBASE, PATH)



def main():
    """ main """
    index = 0
    for csv_table in LIST_OF_FILES:
        index += 1

        #parse data to numpy object
        all_data = expr.MyParser(DBASE, PATH, csv_table)
        if not all_data:
            raise ValueError('type of experiment is unreadable')

        #plot data
        image_name = plot_data(all_data, PATH, csv_table)
        if not image_name:
            print('error')
        print(image_name)
        #well done for one file
        all_data.record_success(DBASE, image_name)
        print('Process: %s/%s ValueErrors: %s' % (index, len(list(LIST_OF_FILES)), all_data.errors))



if __name__ == "__main__":
    main()
    DBASE.close()
